package com.magroun.kata.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.magroun.kata.model.CustomerDeliveryDetails;
import com.magroun.kata.model.User;

public interface CustomerDeliveryDetailsRepository extends JpaRepository<CustomerDeliveryDetails, Integer> {
	List<CustomerDeliveryDetails> findAllByUser(User user);
}

