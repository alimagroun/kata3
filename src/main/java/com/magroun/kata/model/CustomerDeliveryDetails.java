package com.magroun.kata.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

import java.time.LocalDate;

@Entity
public class CustomerDeliveryDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id; 
    private LocalDate deliveryDate;

    private DeliveryMode deliveryMode;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "timeslot_id")
    private TimeSlot timeSlot; 

    public CustomerDeliveryDetails() {
    }

    public CustomerDeliveryDetails(LocalDate deliveryDate, DeliveryMode deliveryMode, User user, TimeSlot timeSlot) {
        this.deliveryDate = deliveryDate;
        this.deliveryMode = deliveryMode;
        this.user = user;
        this.timeSlot = timeSlot;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public DeliveryMode getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(DeliveryMode deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public TimeSlot getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(TimeSlot timeSlot) {
        this.timeSlot = timeSlot;
    }
}

