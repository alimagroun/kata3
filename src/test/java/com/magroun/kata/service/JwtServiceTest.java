package com.magroun.kata.service;

import io.jsonwebtoken.Claims;
import org.junit.jupiter.api.Test;
import java.util.function.Function;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import org.mockito.InjectMocks;

public class JwtServiceTest {
	
    @InjectMocks
    private JwtService jwtService;

    @Test
    void testExtractClaim() {
        JwtService jwtService = mock(JwtService.class);

        Claims mockClaims = mock(Claims.class);
        Function<Claims, String> mockClaimsResolver = claims -> "expectedClaimValue";

        when(jwtService.extractClaim(anyString(), any())).thenCallRealMethod();
        when(jwtService.extractAllClaims(anyString())).thenReturn(mockClaims);

        String extractedClaim = jwtService.extractClaim("sampleToken", mockClaimsResolver);

        assertEquals("expectedClaimValue", extractedClaim, "Extracted claim should match expected claim value");
    }   
}
