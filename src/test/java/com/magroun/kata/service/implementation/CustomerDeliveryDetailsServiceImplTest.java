package com.magroun.kata.service.implementation;

import static org.junit.jupiter.api.Assertions.*;
import java.util.List;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.magroun.kata.repository.CustomerDeliveryDetailsRepository;
import com.magroun.kata.repository.TimeSlotRepository;
import com.magroun.kata.repository.UserRepository;
import com.magroun.kata.model.User;
import com.magroun.kata.model.TimeSlot;
import com.magroun.kata.dto.CustomerDeliveryDetailsDTO;
import com.magroun.kata.model.CustomerDeliveryDetails;
import com.magroun.kata.model.DeliveryMode;
import com.magroun.kata.model.Role;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import static org.junit.jupiter.api.Assertions.assertThrows;
import jakarta.persistence.EntityNotFoundException;

@ExtendWith(MockitoExtension.class)
public class CustomerDeliveryDetailsServiceImplTest {
	
	@Mock
    CustomerDeliveryDetailsRepository deliveryDetailsRepository;

    @Mock
    TimeSlotRepository timeSlotRepository;

    @Mock
    UserRepository userRepository;

    @InjectMocks
    CustomerDeliveryDetailsServiceImpl customerDeliveryDetailsService;
    
    @Test
    void testCreateCustomerDeliveryDetails() {

       UserDetails userDetails = new User("John", "Doe", "john@example.com", "password123", "1234567890", "123 Street", "City", "State", "12345", Role.CUSTOMER);
       Authentication auth = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getAuthorities());
       SecurityContextHolder.getContext().setAuthentication(auth);
        
        User user = new User();
        Mockito.when(userRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.of(user));
        
        TimeSlot savedTimeSlot = new TimeSlot();
        Mockito.when(timeSlotRepository.save(Mockito.any(TimeSlot.class))).thenReturn(savedTimeSlot);
        
        CustomerDeliveryDetails deliveryDetails = new CustomerDeliveryDetails();
        deliveryDetails.setDeliveryDate(LocalDate.of(2024, 01, 03));
        deliveryDetails.setDeliveryMode(DeliveryMode.DELIVERY_TODAY);
        deliveryDetails.setTimeSlot(savedTimeSlot);
        	
        CustomerDeliveryDetailsDTO resultDTO	= customerDeliveryDetailsService.createCustomerDeliveryDetails(deliveryDetails);
        
        verify (deliveryDetailsRepository, times(1)).save(deliveryDetails);
        assertNotNull(resultDTO);
    }
    
    @Test
    public void testGetAllCustomerDeliveryDetailsByUser() {

        UserDetails userDetails = new User("John", "Doe", "john@example.com", "password123", "1234567890", "123 Street", "City", "State", "12345", Role.CUSTOMER);
        Authentication auth = new UsernamePasswordAuthenticationToken(userDetails, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);

        User user = new User();
        Mockito.when(userRepository.findByEmail(Mockito.anyString())).thenReturn(Optional.of(user));

        LocalDate specificDate = LocalDate.of(2024, 1, 21);
        DeliveryMode specificMode = DeliveryMode.DELIVERY_TODAY;
        TimeSlot randomTimeSlot = new TimeSlot();

        CustomerDeliveryDetails deliveryDetails1 = new CustomerDeliveryDetails(specificDate, specificMode, user, randomTimeSlot);
        CustomerDeliveryDetails deliveryDetails2 = new CustomerDeliveryDetails(specificDate, specificMode, user, randomTimeSlot);

        List<CustomerDeliveryDetails> deliveryDetailsList = new ArrayList<>();
        deliveryDetailsList.add(deliveryDetails1);
        deliveryDetailsList.add(deliveryDetails2);

        when(deliveryDetailsRepository.findAllByUser(user)).thenReturn(deliveryDetailsList);

        List<CustomerDeliveryDetailsDTO> result = customerDeliveryDetailsService.getAllCustomerDeliveryDetailsByUser();

        assertEquals(2, result.size());
        verify(deliveryDetailsRepository, times(1)).findAllByUser(user);
    }
    
    @Test
    public void testConvertToDTO() {
    	
    	LocalTime startTime = LocalTime.of(9, 0); // Replace with your desired start time
    	LocalTime endTime = LocalTime.of(11, 0); // Replace with your desired end time

    	TimeSlot timeSlot = new TimeSlot(startTime, endTime);
    	User user = new User();
    	
        CustomerDeliveryDetails deliveryDetails = new CustomerDeliveryDetails(LocalDate.of(2024, 1, 21), DeliveryMode.DELIVERY_TODAY, user ,timeSlot);
        
        CustomerDeliveryDetailsDTO dto = customerDeliveryDetailsService.convertToDTO(deliveryDetails);

        Assertions.assertEquals(deliveryDetails.getDeliveryDate(), dto.deliveryDate());
        Assertions.assertEquals(deliveryDetails.getDeliveryMode(), dto.deliveryMode());
        Assertions.assertEquals(deliveryDetails.getTimeSlot(), dto.timeSlot());
    }
    
    @Test
    public void testDeleteExistingCustomerDeliveryDetails() {
        Integer existingId = 1;

        when(deliveryDetailsRepository.existsById(existingId)).thenReturn(true);

        customerDeliveryDetailsService.deleteCustomerDeliveryDetailsById(existingId);

        verify(deliveryDetailsRepository, times(1)).deleteById(existingId);
    }

    @Test
    public void testDeleteNonExistingCustomerDeliveryDetails() {
        Integer nonExistingId = 2;

        when(deliveryDetailsRepository.existsById(nonExistingId)).thenReturn(false);

        assertThrows(EntityNotFoundException.class,
                () -> customerDeliveryDetailsService.deleteCustomerDeliveryDetailsById(nonExistingId));

        verify(deliveryDetailsRepository, never()).deleteById(nonExistingId);
    }
}