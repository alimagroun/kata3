package com.magroun.kata.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.authentication.AuthenticationManager;

import java.util.Optional;

import com.magroun.kata.repository.TokenRepository;
import com.magroun.kata.repository.UserRepository;
import com.magroun.kata.dto.AuthenticationResponse;
import com.magroun.kata.dto.RegisterRequest;
import com.magroun.kata.dto.AuthenticationRequest;
import com.magroun.kata.model.Role;
import com.magroun.kata.model.User;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;

import org.springframework.http.HttpHeaders;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Mockito.*;

class AuthenticationServiceTest {

    @InjectMocks
    private AuthenticationService authenticationService;

    @Mock
    private UserRepository repository;

    @Mock
    private JwtService jwtService;

    @Mock
    private TokenRepository tokenRepository;

    @Mock
    private PasswordEncoder passwordEncoder;
    
    @Mock
    private AuthenticationManager authenticationManager;
    
    @Mock
    private HttpServletResponse httpResponse;

    @Mock
    private HttpServletRequest httpRequest;

    public AuthenticationServiceTest() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRegister() {
        RegisterRequest registerRequest = createSampleRegisterRequest(); // Create a sample request object

        when(repository.save(any(User.class))).thenReturn(createSampleUser());
        when(jwtService.generateToken(any(User.class))).thenReturn("sampleAccessToken");
        when(jwtService.generateRefreshToken(any(User.class))).thenReturn("sampleRefreshToken");

        AuthenticationResponse response = authenticationService.register(registerRequest);

        assertNotNull(response);
        assertEquals("sampleAccessToken", response.getAccessToken());
        assertEquals("sampleRefreshToken", response.getRefreshToken());

    }

    private RegisterRequest createSampleRegisterRequest() {
        RegisterRequest sampleRequest = new RegisterRequest();
        sampleRequest.setFirstname("John");
        sampleRequest.setLastname("Doe");
        sampleRequest.setEmail("johndoe@example.com");
        sampleRequest.setPassword("samplePassword");
        sampleRequest.setPhoneNumber("1234567890");
        sampleRequest.setStreetAddress("123 Main St");
        sampleRequest.setCity("Sample City");
        sampleRequest.setState("Sample State");
        sampleRequest.setPostalCode("12345");
        sampleRequest.setRole(Role.CUSTOMER);
        return sampleRequest;
    }

    private User createSampleUser() {
        User sampleUser = new User(
            "John",           
            "Doe",             
            "johndoe@example.com", 
            "encodedPassword", 
            "1234567890",       
            "123 Main St",    
            "Sample City",    
            "Sample State",   
            "12345",         
            Role.CUSTOMER           
        );
        return sampleUser;
    }
    
    @Test
    public void testAuthenticate() {
        AuthenticationRequest authenticationRequest = createSampleAuthenticationRequest();
        User sampleUser = createSampleUser();

        when(repository.findByEmail(anyString())).thenReturn(Optional.of(sampleUser));
        when(jwtService.generateToken(any(User.class))).thenReturn("sampleAccessToken");
        when(jwtService.generateRefreshToken(any(User.class))).thenReturn("sampleRefreshToken");

        AuthenticationResponse response = authenticationService.authenticate(authenticationRequest);

        assertNotNull(response);
        assertEquals("sampleAccessToken", response.getAccessToken());
        assertEquals("sampleRefreshToken", response.getRefreshToken());
    }

    private AuthenticationRequest createSampleAuthenticationRequest() {
        AuthenticationRequest sampleRequest = new AuthenticationRequest();
        sampleRequest.setEmail("johndoe@example.com");
        sampleRequest.setPassword("samplePassword");
        return sampleRequest;
    }

    @Test
    public void testRefreshToken() throws IOException {

        when(httpRequest.getHeader(HttpHeaders.AUTHORIZATION)).thenReturn("Bearer sampleRefreshToken");

        when(jwtService.extractUsername("sampleRefreshToken")).thenReturn("user@example.com");
        when(repository.findByEmail(anyString())).thenReturn(Optional.of(createSampleUser()));
        when(jwtService.isTokenValid("sampleRefreshToken", createSampleUser())).thenReturn(true);
        when(jwtService.generateToken(any(User.class))).thenReturn("sampleAccessToken");

        authenticationService.refreshToken(httpRequest, httpResponse);

        verify(repository, times(1)).findByEmail("user@example.com");

    }
}

