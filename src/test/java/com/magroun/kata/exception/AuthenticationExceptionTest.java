package com.magroun.kata.exception;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AuthenticationExceptionTest {

    @Test
    public void testExceptionMessage() {
        String errorMessage = "Authentication failed!";
        AuthenticationException exception = new AuthenticationException(errorMessage);

        assertNotNull(exception);
        assertEquals(errorMessage, exception.getMessage());
    }
    
    @Test
    public void testEmptyMessage() {
        AuthenticationException exception = new AuthenticationException("");

        assertNotNull(exception);
        assertEquals("", exception.getMessage());
    }
}
