package com.magroun.kata.exception;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class UserNotFoundExceptionTest {

    @Test
    public void testExceptionMessage() {
        String errorMessage = "User not found!";
        UserNotFoundException exception = new UserNotFoundException(errorMessage);

        assertNotNull(exception);
        assertEquals(errorMessage, exception.getMessage());
    }

    @Test
    public void testEmptyMessage() {
        UserNotFoundException exception = new UserNotFoundException("");

        assertNotNull(exception);
        assertEquals("", exception.getMessage());
    }

}
