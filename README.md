# Kata

## Description

This project enables users to perform various actions such as signing up, signing in, signing out, creating delivery requests, managing these requests, and interacting with the system.

## Features

- **User Authentication:** Allows users to sign up, sign in, and sign out securely. Utilizes token-based authentication, including refresh tokens for seamless session management without frequent reauthentication.
- **Create Delivery Request:** Logged-in users can create delivery requests, specifying date, delivery mode, and time slot.
- **View Deliveries:** Users can view all their delivery requests.
- **Delete Delivery Request:** Users have the ability to delete specific delivery requests.

## Implementation Details

This project is developed using Spring Boot with Java 21 and MySQL as the database.

### Potential Subtleties

- Ensure secure user authentication to protect user data.
- Implement efficient data retrieval for users' delivery history.

## How to Launch the Project

### Database Setup

Set up MySQL and configure the database settings in the application properties file.

### Build and Run

Build the project using your preferred IDE or with the following command:

`mvn clean install`

Run the project:

`mvn spring-boot:run`

### Access the Application

Once the project is running, access it via the specified URL:

`http://localhost:8080`
